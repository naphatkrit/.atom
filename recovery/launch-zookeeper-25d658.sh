#!/bin/bash -eu

RUNFILES=$(echo $0.runfiles)/__main__
if [ ! -d $RUNFILES ]; then
  RUNFILES=`pwd`
fi

DATADIR=/tmp/zkdatadir
mkdir -p $DATADIR

# If a datadir is provided, copy it.
if [ -n "${DATADIR_SRC:-}" ]; then
  cp -r $RUNFILES/$DATADIR_SRC $DATADIR
fi

ZK_PORT=${ZK_PORT:-2181}
cp $RUNFILES/dropbox/isotester/services/zookeeper/log4j.properties $DATADIR/log4j.properties
echo -e "dataDir=$DATADIR\nclientPort=$ZK_PORT\n" > $DATADIR/zoo.cfg
export ZKCFGDIR=$DATADIR

ZKLIBS=$RUNFILES/external/zookeeper_bins/lib
exec java -Dzookeeper.log.dir=. -Dzookeeper.root.logger=INFO,CONSOLE -cp $ZKLIBS/slf4j-log4j12-1.6.1.jar:$ZKLIBS/slf4j-api-1.6.1.jar:$ZKLIBS/netty-3.7.0.Final.jar:$ZKLIBS/log4j-1.2.16.jar:$ZKLIBS/jline-0.9.94.jar:$RUNFILES/external/zookeeper_bins/zookeeper-3.4.6.jar:$DATADIR: -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.local.only=false org.apache.zookeeper.server.quorum.QuorumPeerMain $DATADIR/zoo.cfg
