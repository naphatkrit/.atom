##A Zenburn theme for Atom.

This theme is loosely based on the the theme found [here](https://github.com/StylishThemes/GitHub-Dark/blob/master/themes/pygments-zenburn.min.css).

Screenshot(s)
===
![screen](http://i.imgur.com/wmcxeqN.png)
