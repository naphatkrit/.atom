# Unity Dark Syntax Theme

Atom syntax theme that blends with the [Unity Dark UI](https://atom.io/themes/unity-dark-ui).

##Color Palette

![Screenshot](https://raw.githubusercontent.com/defreitasdudu/unity-dark-syntax/master/unity-dark-syntax.png)
<br>*Colors are customizable with LESS*

##Installation

```
cd ~/.atom/packages
git clone https://github.com/defreitasdudu/unity-dark-syntax --depth=1
```

The theme is also available in the Atom repositories, so you can install directly through the Atom Settings.
